<?php
/***********************************************************
 * 频道模型
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\common\model;
use think\facade\Db;
use think\facade\Cache;
class ChannelType
{

    // 获取单条记录
    public function getInfo($id)
    {
        $cacheKey = "channeltype_getInfo_{$id}";
        $result = cache::get($cacheKey);
        if(empty($result)){
            $result = Db::name('channel_type')
                    ->field('*')
                    ->where('id',$id)
                    ->find(); 
            Cache::tag('channeltype')->set($cacheKey, $result);           
        }
        return $result;
    }

    // 获取单条记录
    public function getInfoByWhere($where, $field = '*')
    {
        $result = Db::name('channel_type')->field($field)->where($where)->find();
        return $result;
    }

    // 获取多条记录
    public function getListByIds($ids, $field = '*')
    {
        $map = [];
        $map[] = ['id','IN',$ids];
        $result = Db::name('channel_type')
                ->field($field)
                ->where($map)
                ->order('sort_order asc')
                ->select()->toArray();
        return $result;
    }

    // 默认获取全部
    public function getAll($field = '*', $map = [], $index_key = '')
    {
        $cacheKey = ['common','model','Channeltype','getAll',$field,$map,$index_key];
        $cacheKey = json_encode($cacheKey);
        $result = cache::get($cacheKey);
        if (empty($result)) {
            $result = Db::name('channel_type')->field($field)
                        ->where($map)
                        ->order('sort_order asc, id asc')
                        ->select()->toArray();
            if (!empty($index_key)) {
                $result = convert_arr_key($result, $index_key);
            }
            Cache::tag('channeltype')->set($cacheKey, $result);
        }
        return $result;
    }

    // 根据文档ID获取模型信息
    public function getInfoByAid($aid)
    {
        $result = [];
        $res1 = Db::name('archives')->where('aid',$aid)->find(); // 主表文档内容
        $res2 = Db::name('channel_type')->where('id',$res1['channel'])->find();
        if (is_array($res1) && is_array($res2)) {
            $result = array_merge($res1, $res2);
        }
        return $result;
    }

    
}