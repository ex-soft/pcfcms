<?php
/***********************************************************
 * 字段模型
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\admin\model;
use think\facade\Db;
use think\facade\Session;
use think\facade\Request;
class Channelfield extends Common
{
    //字段列表
    public function tableData($post){
        if(isset($post['limit'])){
            $limit = $post['limit'];
        }else{
            $limit = 10;
        }
        $tableWhere = $this->pcftableWhere($post);
        $list = Db::name('channelfield')
              ->field($tableWhere['field'])
              ->where($tableWhere['where'])
              ->order($tableWhere['order'])
              ->paginate($limit)->toArray();
        $pcfdata = $list['data'];
        foreach ($pcfdata as $key => $value) {
            $pcfdata[$key]['dtypename'] = Db::name('field_type')->where('name', $value['dtype'])->value('title');
            $pcfdata[$key]['add_time'] = pcftime($value['add_time']);
            $pcfdata[$key]['update_time'] = pcftime($value['update_time']);
        }
        $result = ['code' => 0, 'msg' => 'ok','count' =>$list['total'],'data' => $pcfdata];
        return $result;
    }

    protected function pcftableWhere($post){
        $where = [];
        if(isset($post['channel_id']) && !empty($post['channel_id']) && $post['channel_id'] > 0){
            $where[] = ['channel_id', '=', $post['channel_id']];
        }
        $where[] = ['ifcontrol', '=', 0]; // 不显示系统内置字段
        $result['where'] = $where;
        $result['field'] = "*";
        $result['order'] = "sort_order asc, ifmain asc, ifcontrol asc, id desc";
        return $result;
    }

}
