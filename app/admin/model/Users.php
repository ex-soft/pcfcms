<?php
/***********************************************************
 * 会员管理
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\admin\model;
use think\facade\Db;
use think\facade\Request;
class Users extends Common
{
    //列表
    public function tableData($post)
    {
        if(isset($post['limit'])){
            $limit = $post['limit'];
        }else{
            $limit = 10;
        }
        $tableWhere = $this->pcftableWhere($post);
        $list = Db::name('users')
                ->field($tableWhere['field'])
                ->where($tableWhere['where'])
                ->order($tableWhere['order'])
                ->paginate($limit)->toArray();
        $data = $list['data'];
        foreach ($data as $key=>$val){
            $level_name = Db::name("users_level")->where([['id','=',$val['level_id']]])->field('level_name')->find();
            $data[$key]['level_name'] =  $level_name['level_name'];
            $data[$key]['add_time'] = pcftime($val['add_time']);
            $data[$key]['login_time'] = pcftime($val['login_time']);
            $data[$key]['litpic'] = get_head_pic($val['litpic']);
        }
        $result = ['code' => 0, 'msg' => 'ok','count' =>$list['total'],'data' => $data];
        return $result;
    }
    
    protected function pcftableWhere($post)
    {
        $where = [];
        if (isset($post['username']) && $post['username'] != "") {
            $where[] = ['username', 'like', '%' . $post['username'] . '%'];
        }
        if (isset($post['moblie']) && $post['moblie'] != "") {
            $where[] = ['mobile', '=', $post['moblie']];
        }
        if (isset($post['level_id']) && $post['level_id'] != "") {
            $where[] = ['level_id', '=', $post['level_id']];
        }
        $result['where'] = $where;
        $result['field'] = "*";
        $result['order'] = "id desc";
        return $result;
    }

    //添加/编辑
    public function toAdd($data)
    {
        //判断是新增还是修改
        if (isset($data['id']) && !empty($data['id'])) {
            $edit_data = [];
            if (!$data['mobile']){
                $result = ['status' => false,'msg'=> '请填写手机号码'];
                return $result;
            }
            $where1 = [];
            $where1[] = ['mobile','=',$data['mobile']];
            $where1[] = ['id', '<>', $data['id']];
            if(Db::name('users')->where($where1)->count('id') > 0){
                $result = ['status' => false,'msg'=> '该手机号码已经被使用！'];
                return $result;
            }
            if($data['password']){
               $edit_data['password'] = func_encrypt($data['password']);
            }
            $edit_data['mobile'] = $data['mobile'];
            $edit_data['email'] = $data['email']; 
            if(!empty($edit_data['email'])){
               $edit_data['is_email'] = 1;
            }else{
               $edit_data['is_email'] = 0; 
            }
            $edit_data['is_mobile'] = 1;
            $edit_data['price'] = $data['price'];
            $edit_data['level_id'] = $data['level_id'];
            $edit_data['nickname'] = $data['nickname'] ? $data['nickname']:'游客';
            $edit_data['truename'] = $data['truename'];
            $edit_data['id'] = $data['id'];
            $edit_data['qq']  = $data['qq'];
            $edit_data['remark']  = $data['remark'];
            $edit_data['update_time'] = getTime();
            if (Db::name('users')->save($edit_data)) {
                $result = ['status' => true,'msg'=> '修改成功','url'=>Request::baseFile().'/users/index'];
                return $result;

            } else {
                $result = ['status' => false,'msg'=> '修改失败'];
                return $result;
            }
        } else {
            $add_data = [];
            if (!$data['mobile']){
                $result = ['status' => false,'msg'=> '请填写手机号码'];
                return $result;
            }
            $where1 = [];
            $where1[] = ['mobile','=',$data['mobile']];
            if(Db::name('users')->where($where1)->count('id') > 0){
                $result = ['status' => false,'msg'=> '该手机号码已经被使用！'];
                return $result;
            }
            if (!$data['password']){
                $result = ['status' => false,'msg'=> '请填写密码'];
                return $result;
            }
            $add_data['username'] = get_rand_str(1,0,1).get_rand_str(5,0,2);
            $add_data['password'] = func_encrypt($data['password']);
            $add_data['mobile'] = $data['mobile'];
            $add_data['email'] = $data['email']; 
            if(!empty($add_data['email'])){
               $add_data['is_email'] = 1;
            }else{
               $add_data['is_email'] = 0; 
            }
            $add_data['nickname'] = $data['nickname'] ? $data['nickname']:'游客';
            $add_data['truename'] = $data['truename'];
            $add_data['qq']  = $data['qq']; 
            $add_data['register_place'] = 1;
            $add_data['is_mobile'] = 1;
            $add_data['price'] = $data['price'];
            $add_data['level_id'] = $data['level_id'];
            $add_data['remark'] = $data['remark'];
            $add_data['add_time'] = getTime();
            $add_data['login_time'] = getTime();
            if (Db::name('users')->save($add_data)) {
                $result = ['status' => true,'msg'=> '添加成功','url'=>Request::baseFile().'/users/index'];
                return $result;
            } else {
                $result = ['status' => false,'msg'=> '添加失败'];
                return $result;
            }
        }
    }


}
