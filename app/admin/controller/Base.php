<?php
/***********************************************************
 * 基础控制器
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\admin\controller;
use think\facade\Session;
use app\BaseController;
class Base extends BaseController
{
    protected function _initialize()
    {
        parent::_initialize();
        //获取当前用户是否登录
        $admin_id = Session::get('admin_id');
        if(empty($admin_id)){
            $this->redirect(url('/login/index')->suffix(false)->domain(true)->build());
        }
        //过滤不需要登录的操作
        $filter_login_action = [
            'Login/index',      //登录页面
            'Login/captcha',    //登录验证码
            'Login/logout',     //退出登录
        ];
        //过滤不需要登陆的行为
        $ctl_act = $this->request->controller().'/'.$this->request->action();
        $ctl_all = $this->request->controller().'/*';
        if (!in_array($ctl_act, $filter_login_action) || !in_array($ctl_all, $filter_login_action)) {
            $admin_login_expire = session::get('admin_login_expire'); // 登录有效期
            if (!empty($admin_id) && ((getTime() - intval($admin_login_expire)) < config('params.login_expire'))) {
                session::set('admin_login_expire', getTime()); // 登录有效期
            }else{
                session_unset();
                $domain = url('/login/index')->suffix(false)->domain(true)->build();
                $this->redirect($domain); 
            }
        }
    }

    //提醒页面
    public function Notice($msg = '操作失败',$backUrl = false,$wait = 3 ,$type = false)
    {
        $gobackurl = url('/index/main')->suffix(true)->domain(false)->build();
        $this->assign(compact('msg','backUrl','wait','type'));
        $this->assign('gobackurl',$gobackurl);
        return $this->fetch('public/message');
    }

}
