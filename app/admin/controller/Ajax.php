<?php
/***********************************************************
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\admin\controller;
use think\facade\Db;
use think\facade\Request;
use think\facade\Session;
use think\facade\Cache;
use app\admin\logic\AjaxLogic;
class Ajax extends Base 
{
    private $ajaxLogic;
    public function _initialize() {
        parent::_initialize();
        $this->ajaxLogic = new AjaxLogic;
    }
    
    //进入欢迎页面需要异步处理的业务
    public function welcome_handle(){
        $this->ajaxLogic->welcome_handle();
    }

    //一般修改状态 比如 是否推荐 是否开启 等 图标切换的
    public function ajax_changeTableVal(){
        if (Request::isAjax()) {
            $param = input('param.');
            $table    = input('param.table/s'); // 表名
            $id_name  = input('param.id_name/s'); // 表主键id名
            $id_value = input('param.id_value/d'); // 表主键id值
            $field    = input('param.field/s'); // 修改哪个字段
            $value    = input('param.value/s','', null); // 修改字段值
            $value    = pcfPreventShell($value) ? $value : strip_sql($value);
            
            $openstatus    = input('param.openstatus/d',0);
            if($openstatus == 1){
                $result = ['status' => false, 'msg' => '不允许操作'];
                return $result;               
            }elseif($openstatus == 2){
                $row = Db::name('channel_type')->where('id', $id_value)->find();
                if (!empty($row) && in_array($row['id'], [1,6])) {
                    $result = ['status' => false,'msg'=> '系统内置模型，禁止操作！'];
                    return $result;
                }              
            }elseif($openstatus == 3){
                $row = Db::name('channelfield')->where('id', $id_value)->find();
                if (!empty($row) && 1 == intval($row['ifcontrol'])) {
                    $result = ['status' => false,'msg'=> '系统内置表单，禁止操作！'];
                    return $result;
                }
            }
            if(isset($param['act'])){
                $ctl_act    = $param['act'];
                $popedom = appfile_popedom($ctl_act);
                if(!$popedom["status"]){
                    if(config('params.auth_msg.test')){
                        $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                        return $result;
                    }else{
                        $result = ['status' => false, 'msg' => config('params.auth_msg.status')];
                        return $result;                    
                    }
                }
            }
            unset($param['act']);
            //处理数据的安全性
            if (empty($id_value)) {
                $result = ['status' => false, 'msg' => '查询条件id不合法！'];
                return $result;
            }
            foreach ($param as $key => $val) {
                if ('value' == $key) {continue;}
                if (!preg_match('/^([A-Za-z0-9_-]*)$/i', $val)) {
                    $result = ['status' => false, 'msg' => '数据含有非法入侵字符！'];
                    return $result;
                }
            }
            $savedata = [$field => $value,'update_time' => getTime()];
            $r = Db::name($table)->where($id_name,$id_value)->save($savedata);
            if($r){
                Cache::clear('getMenuglobal');//清空后台导航缓存
                $result = ['status' => true, 'msg' => '更新成功'];                  
            }else{
                $result = ['status' => false, 'msg' => '更新失败'];                 
            }
            return $result;
        }
    }

    // 文档内容里面设置作者默认名称 
    public function ajax_setfield(){
        if (Request::isAjax()) {
            $admin_id = session::get('admin_id');
            $field  = input('field'); // 修改哪个字段
            $value  = input('value', '', null); // 修改字段值  
            if (!empty($admin_id)) {
                $r = Db::name('admin')->where('admin_id',intval($admin_id))->save([$field=>$value,'update_time'=>getTime()]);
                if ($r) {
                    // 更新存储在session里的信息
                    $admin_info = session::get('admin_info');
                    $admin_info[$field] = $value;
                    session::set('admin_info', $admin_info);
                    $result = ['status' => true, 'msg' => '操作成功'];
                    return $result;
                }
            }
        }
        $result = ['status' => false, 'msg' => '操作失败'];
        return $result;
    }

}