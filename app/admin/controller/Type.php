<?php
/***********************************************************
 * 模型管理
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\admin\controller;
use think\facade\Db;
use think\facade\Cache;
use think\facade\Request;
use app\admin\model\ChannelType as ChannelTypeModel;
class Type extends Base
{

    // 系统默认的模型ID，不可删除
    private $channeltype_system_id = [];
    // 系统内置不可用的模型标识，防止与home分组的控制器重名覆盖
    private $channeltype_system_nid = ['base','index','lists','view','search','error','single'];
    public $ChannelTypeModel;
    public $popedom;
    public function _initialize() {
        parent::_initialize();
        $this->channeltype_system_nid = array_merge($this->channeltype_system_nid);
        $this->channeltype_system_id = Db::name('channel_type')->where('ifsystem',1)->column('id');
        $this->ChannelTypeModel = new ChannelTypeModel();
        $ctl_act = strtolower(Request::controller().'/index');
        $this->popedom = appfile_popedom($ctl_act);
    }
    
    // 列表
    public function index(){ 
        if(!$this->popedom["list"]){
            return $this->Notice(config('params.auth_msg.list'),true,3,false);
        }
        if (Request::isAjax()) {
            return $this->ChannelTypeModel->tableData(input('param.'));
        }
        return $this->fetch();
    }

    // 添加
    public function add(){
        if (Request::isPost()) {
            if(!$this->popedom["add"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.add')];
                    return $result;                    
                }
            }
            return $this->ChannelTypeModel->toAdd(input('param.'),$this->channeltype_system_nid);
        }
        return $this->fetch();
    }

    // 编辑
    public function edit(){
        $channelinfo = Db::name('channel_type')->where('id',input('param.id/d'))->find();
        if (Request::isPost()) {
            if(!$this->popedom["modify"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.modify')];
                    return $result;                    
                }
            }
            return $this->ChannelTypeModel->toAdd(input('param.'),$this->channeltype_system_nid);
        }
        $this->assign('field', $channelinfo);
        return $this->fetch(); 
    }

    // 删除
    public function del(){
        $id_arr = input('param.id/d');
        if (Request::isAjax()) {
            if(!$this->popedom["delete"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.delete')];
                    return $result;                    
                }
            }
            if(!empty($id_arr)){
                if (array_key_exists($id_arr, $this->channeltype_system_id)) {
                    $result = ['status' => false, 'msg' => '系统内置模型，禁止删除！'];
                    return $result; 
                }
                $presult = Db::name('channel_type')->field('id,title,nid,ctl_name')->where("id",$id_arr)->select()->toArray();
                $title_list = get_arr_column($presult, 'title');
                $r = Db::name('channel_type')->where("id",$id_arr)->delete();
                if ($r) {
                    // 删除栏目
                    $arctype = Db::name('arctype')->where('channeltype|current_channel',$id_arr)->delete();
                    // 删除文章
                    $archives = Db::name('archives')->where("channel",$id_arr)->delete();
                    // 删除自定义字段
                    $channelfield = Db::name('channelfield')->where("channel_id",$id_arr)->delete();
                    // 删除相关文件和数据
                    foreach ($presult as $key => $value) {
                        $nid = $value['nid'];
                        // 删除相关数据表
                        try {
                            Db::execute('DROP TABLE '.config('database.connections.mysql.prefix').$nid.'_content');
                        } catch (\Exception $e) {}
                        $filelist_path = WWW_ROOT.'extend/pcfcms/custom_path/'.$nid.'.filelist.txt';
                        if(file_exists($filelist_path)){
                            $fileStr = file_get_contents($filelist_path);
                            $filelist = explode("\n\r", $fileStr);
                            foreach ($filelist as $k1 => $v1) {

                                if($k1 == 0 || $k1 == 1){
                                   $v1 = trim(WWW_ROOT.$v1);
                                }else{
                                   $v1 = trim(WWW_ROOT.'public/'.$v1);
                                }
                                if (!empty($v1) && file_exists($v1)){@unlink($v1);}

                            }    
                        }
                        @unlink($filelist_path);
                    }
                    Cache::clear();
                    $result = ['status' => true, 'msg' => '删除成功'];
                    return $result; 
                }
                $result = ['status' => false, 'msg' => '删除失败'];
                return $result; 
            }
            $result = ['status' => false, 'msg' => '非法访问'];
            return $result; 
        }
        $result = ['status' => false, 'msg' => '非法访问'];
        return $result; 
    }
}
