<?php
/***********************************************************
 * 上传管理
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\admin\controller;
use think\File;
use think\facade\Db;
use think\facade\Request;
use think\facade\Session;
use think\facade\Filesystem;
class Ueditor extends Base
{
    
    private $sub_name = array('date', 'Ymd');
    private $fileExt = "jpg,png,gif,jpeg,bmp,ico";
    private $nowFileName = "";

    public function _initialize()
    {
        parent::_initialize();
        date_default_timezone_set("Asia/Shanghai");
        $this->nowFileName = input('nowfilename', '');
        if (empty($this->nowFileName)) {
            $this->nowFileName = md5(time().uniqid(mt_rand(), true));
        }
        error_reporting(E_ERROR | E_WARNING);
        header("Content-Type: text/html; charset=utf-8");
        $image_type = sysConfig('basic.image_type');
        $this->fileExt = !empty($image_type) ? str_replace('|', ',', $image_type) : $this->fileExt;
    }
    
    public function index()
    {
        $CONFIG2 = json_decode(preg_replace("/\/\*[\s\S]+?\*\//","",file_get_contents(WWW_ROOT."public/common/plugins/Ueditor/php/config.json")),true);
        $action = $_GET['action'];
        switch ($action) {
            case 'config':
                $result =  json_encode($CONFIG2);
                break;
            /* 上传图片 */
            case 'uploadimage':
                $fieldName = $CONFIG2['imageFieldName'];
                $result = $this->upFile($fieldName);
                break;
            /* 上传涂鸦 */
            case 'uploadscrawl':
                $config = array(
                    "pathFormat" => $CONFIG2['scrawlPathFormat'],
                    "maxSize" => $CONFIG2['scrawlMaxSize'],
                    "allowFiles" => $CONFIG2['scrawlAllowFiles'],
                    "oriName" => "scrawl.png"
                );
                $fieldName = $CONFIG2['scrawlFieldName'];
                $base64 = "base64";
                $result = $this->upBase64($config,$fieldName);
                break;
            /* 上传视频 */
            case 'uploadvideo':
                $fieldName = $CONFIG2['videoFieldName'];
                $result = $this->upFile($fieldName);
                break;
            /* 上传文件 */
            case 'uploadfile':
                $fieldName = $CONFIG2['fileFieldName'];
                $result = $this->upFile($fieldName);
                break;
            /* 列出图片 */
            case 'listimage':
                $allowFiles = $CONFIG2['imageManagerAllowFiles'];
                $listSize = $CONFIG2['imageManagerListSize'];
                $path = $CONFIG2['imageManagerListPath'];
                $get =$_GET;
                $result =$this->fileList($allowFiles,$listSize,$get);
                break;
            /* 列出文件 */
            case 'listfile':
                $allowFiles = $CONFIG2['fileManagerAllowFiles'];
                $listSize = $CONFIG2['fileManagerListSize'];
                $path = $CONFIG2['fileManagerListPath'];
                $get = $_GET;
                $result = $this->fileList($allowFiles,$listSize,$get);
                break;
            /* 抓取远程文件 */
            case 'catchimage':
                $config = [
                    "pathFormat" => $CONFIG2['catcherPathFormat'],
                    "maxSize" => $CONFIG2['catcherMaxSize'],
                    "allowFiles" => $CONFIG2['catcherAllowFiles'],
                    "oriName" => "remote.png"
                ];
                $fieldName = $CONFIG2['catcherFieldName'];
                $list =[];
                isset($_POST[$fieldName]) ? $source = $_POST[$fieldName] : $source = $_GET[$fieldName];
                foreach($source as $imgUrl){
                    $info = json_decode($this->saveRemote($config,$imgUrl),true);
                    array_push($list, [
                        "state" => $info["state"],
                        "url" => $info["url"],
                        "size" => $info["size"],
                        "title" => htmlspecialchars($info["title"]),
                        "original" => str_replace("&amp;", "&", htmlspecialchars($info["original"])),
                        "source" => str_replace("&amp;", "&", htmlspecialchars($imgUrl))
                    ]);
                }
                $result = json_encode([
                    'state' => !empty($list) ? 'SUCCESS' : 'ERROR',
                    'list' => $list
                ]);
                break;
            default:
                $result = json_encode(['state' => '请求地址出错']);
                break;
        }
        if(isset($_GET["callback"])){
            if(preg_match("/^[\w_]+$/", $_GET["callback"])){
                echo htmlspecialchars($_GET["callback"]).'('.$result.')';
            }else{
                echo json_encode(['state' => 'callback参数不合法']);
            }
        }else{
            echo $result;
        }
    }

    // 其他上传 图片
    public function imageUp()
    {
        //文件上传到哪里       
        if (!Request::isAjax()) {
            $return_data['state'] = '非法上传';
            respose($return_data,'json');
        }
        $image_upload_limit_size = intval(sysConfig('basic.file_size') * 1024 * 1024);

        // 获取表单上传文件
        $file = request()->file('file');
        if(empty($file)){
            $file = request()->file('upfile');
        }
        $tptype = pathinfo($file->getoriginalName(), PATHINFO_EXTENSION);
        $tptype = strtolower($tptype);
        $newfileExt = explode(",",$this->fileExt);

        //ico图片文件不进行验证
        if ($tptype != 'ico') {
            if ($image_upload_limit_size <= $file->getSize()) {
                $state = "ERROR：上传文件过大";
                $return_data['state'] = $state;
                return $return_data;
            }
            if (!in_array($tptype, $newfileExt)) {
                $state = "ERROR：上传文件后缀名必须为".$this->fileExt;
                $return_data['state'] = $state;
                return $return_data; 
            }
        }

        //验证图片一句话木马
        $imgstr = @file_get_contents($file->getoriginalName());
        if (false !== $imgstr && preg_match('#<\?php#i', $imgstr)) {
            $state = "ERROR：上传图片不合格";
            $return_data['state'] = $state;
            return $return_data;
        }

        if (!empty($file)) {
            $ossConfig = sysConfig('oss');
            if ($ossConfig['oss_switch'] > 0) {
                //这是上传到第三方图片库
            }else{
                $fileName = Filesystem::disk('local')->putFile('allimg', $file);
                if ($fileName) {
                    $state = "SUCCESS";
                }else{
                    $state = "ERROR" . '上传图片失败！';
                }
                $fileName = str_replace("\/", "/", $fileName);
                $fileName = str_replace("\\", "/", $fileName);   
            }
        } 

        if($state == 'SUCCESS' && pathinfo($file->getoriginalName(), PATHINFO_EXTENSION) != 'ico')
        {
            // 水印功能
            $imgresource = "./uploads/".$fileName;
            $image = \think\Image::open($imgresource);
            $water = sysConfig('water');
            $return_data['mark_type'] = $water['mark_type'];
            if($water['is_mark']==1 && $image->width()>$water['mark_width'] && $image->height()>$water['mark_height']){
                if($water['mark_type'] == 'text'){
                    $ttf = WWW_ROOT.'public/common/font/hgzb.ttf';
                    if (file_exists($ttf)) {
                        $size = $water['mark_txt_size'] ? $water['mark_txt_size'] : 30;
                        $color = $water['mark_txt_color'] ? $water['mark_txt_color'] : '#000000';
                        if (!preg_match('/^#[0-9a-fA-F]{6}$/', $color)) {
                            $color = '#000000';
                        }
                        $transparency = intval((100 - $water['mark_degree']) * (127/100));
                        $color.= dechex($transparency);
                        $image->open($imgresource)->text($water['mark_txt'], $ttf, $size, $color, $water['mark_sel'])->save($imgresource);
                        $return_data['mark_txt'] = $water['mark_txt'];
                    }
                }else{
                    $water['mark_img'] = preg_replace('#^(/[/\w]+)?(/public/uploads/|/uploads/)#i', '$2', $water['mark_img']); // 支持子目录
                    $waterPath = public_path().$water['mark_img'];
                    $waterPath = str_replace("\/", "/", $waterPath);
                    if (pcfPreventShell($waterPath) && file_exists($waterPath)) {
                        $quality = $water['mark_quality'] ? $water['mark_quality'] : 80;
                        $waterTempPath = dirname($waterPath).'/temp_'.basename($waterPath);
                        $image->open($waterPath)->save($waterTempPath, null, $quality);
                        $image->open($imgresource)->water($waterTempPath, $water['mark_sel'], $water['mark_degree'])->save($imgresource);
                        @unlink($waterTempPath);
                    }
                }
            }
            // 水印功能

            $iData['pid']    = md5('/uploads/'.$fileName);
            $iData['type']  = config('filesystem.default');
            $iData['name']  = $file->getoriginalName();
            $iData['url']   = '/uploads/'.$fileName;
            $iData['ctime'] = time();
            $iData['path']  = '/uploads/'.$fileName;
            $pcfcmsuppic = Db::name('images')->save($iData);
            if($pcfcmsuppic){
                $state = "SUCCESS"; 
            }else{
                $state = "ERROR" . '图片存入数据库失败！';
            }
        }

        $return_data['url'] = '/uploads/'.$fileName; 
        $return_data['state'] = $state;
        return $return_data;
    }

    // 编辑器上传 图片
    private function upFile($fieldName)
    {
        $file = request()->file($fieldName);
        if(empty($file)){
            return json_encode(['state' =>'ERROR，请上传文件']);
        }
        $image_upload_limit_size = intval(sysConfig('basic.file_size') * 1024 * 1024);
        $fileExt = '';
        $image_type = sysConfig('basic.image_type');
        !empty($image_type) && $fileExt .= '|'.$image_type;
        $file_type = sysConfig('basic.file_type');
        !empty($file_type) && $fileExt .= '|'.$file_type;
        $media_type = sysConfig('basic.media_type');
        !empty($media_type) && $fileExt .= '|'.$media_type;
        $fileExt = !empty($fileExt) ? str_replace('||', '|', $fileExt) : "jpg,png,gif,jpeg,bmp,ico";
        $fileExt = str_replace('|', ',', trim($fileExt, '|'));
        $tptype = pathinfo($file->getoriginalName(), PATHINFO_EXTENSION);
        $tptype = strtolower($tptype);
        $newfileExt = explode(",",$fileExt);
        if ($image_upload_limit_size <= $file->getSize()) {
            $state = "ERROR：上传文件过大";
            return json_encode(['state' =>$state]);
        }
        if (!in_array($tptype, $newfileExt)) {
            $state = "ERROR：上传文件后缀名必须为".$fileExt;
            return json_encode(['state' =>$state]);
        }
        $ossConfig = sysConfig('oss');
        if ($ossConfig['oss_switch'] >0) {
            $uptype = "Aliyun";
        } else {
            $uptype = "local";
            $fileName = Filesystem::disk('local')->putFile('ueditor', $file);
            if ($fileName) {
                $state = "SUCCESS";
            } else {
                $state = "ERROR" . '上传图片失败！';
            }
            $fileName = str_replace("\/", "/", $fileName);
            $fileName = str_replace("\\", "/", $fileName);   
        }
        if($state == 'SUCCESS'){
            $data = array(
                'state' => 'SUCCESS',
                'url' => '/uploads/'.$fileName,
                'title' => $file->getoriginalName(),
                'original' => $file->getoriginalName(),
                'type' => '.' . $tptype,
                'size' => $file->getSize(),
            );
            //图片加水印
            if($data['state'] == 'SUCCESS'){
                $file_type = $tptype;
                if ($file_type && 'ico' != $file_type) {
                    $imgresource = "./uploads/".$fileName;

                    // 添加水印
                    $image = \think\Image::open($imgresource);
                    $water = sysConfig('water');
                    $return_data['mark_type'] = $water['mark_type'];
                    if($water['is_mark']==1 && $image->width()>$water['mark_width'] && $image->height()>$water['mark_height']){
                        if($water['mark_type'] == 'text'){
                            $ttf = public_path().'public/common/font/hgzb.ttf';
                            if (file_exists($ttf)) {
                                $size = $water['mark_txt_size'] ? $water['mark_txt_size'] : 30;
                                $color = $water['mark_txt_color'] ? $water['mark_txt_color'] : '#000000';
                                if (!preg_match('/^#[0-9a-fA-F]{6}$/', $color)) {
                                    $color = '#000000';
                                }
                                $transparency = intval((100 - $water['mark_degree']) * (127/100));
                                $color.= dechex($transparency);
                                $image->open($imgresource)->text($water['mark_txt'], $ttf, $size, $color, $water['mark_sel'])->save($imgresource);
                                $return_data['mark_txt'] = $water['mark_txt'];
                            }
                        }else{
                            $water['mark_img'] = preg_replace('#^(/[/\w]+)?(/public/uploads/|/uploads/)#i', '$2', $water['mark_img']); // 支持子目录
                            $waterPath = public_path()."public".$water['mark_img'];
                            $waterPath = str_replace("\/", "/", $waterPath);
                            if (pcfPreventShell($waterPath) && file_exists($waterPath)) {
                                $quality = $water['mark_quality'] ? $water['mark_quality'] : 80;
                                $waterTempPath = dirname($waterPath).'/temp_'.basename($waterPath);
                                $image->open($waterPath)->save($waterTempPath, null, $quality);
                                $image->open($imgresource)->water($waterTempPath, $water['mark_sel'], $water['mark_degree'])->save($imgresource);
                                @unlink($waterTempPath);
                            }
                        }
                    }

                    $iData['pid']    = md5('/uploads/'.$fileName);
                    $iData['type']  = $uptype;
                    $iData['name']  = $file->getoriginalName();
                    $iData['url']   = '/uploads/'.$fileName;
                    $iData['ctime'] = time();
                    $iData['path']  = '/uploads/'.$fileName;
                    $pcfcmsuppic = Db::name('images')->save($iData);
                    if($pcfcmsuppic){
                        $state = "SUCCESS"; 
                    }else{
                        $state = "ERROR" . '上传图片失败！';
                    }


                }
            }
        }else{
            $data = ['state' => 'ERROR'];
        }
        return json_encode($data);
    }

    // 远程图片本地化 
    public function ajax_remote_to_local()
    {
        $result = ['status' => false,'msg' => '本地化失败！'];
        if (Request::isAjax()) {
            $body = input('post.body/s');
            $body = remote_to_local($body);
            $result = ['status' => true, 'msg' => '本地化成功！','body' =>$body];
            return $result;
        }
        return $result; 
    }

    // 清除非站内链接 
    public function ajax_replace_links()
    {
        if (Request::isAjax()) {
            $body = input('post.body/s');
            $body = replace_links($body);
            $result = ['status' => true, 'msg' => '清除成功','body' =>$body];
            return $result;  
        }
        $result = ['status' => false, 'msg' => '清除失败！'];
        return $result;
    }

    //图库在线管理 - 左侧树形目录结构
    public function picture_folder()
    {
        $func       = input('func/s', '');
        $num        = input('num/d', 1);   
        $inputId    = input('inputId/s', '');
        $info = [
            'num'      => $num,
            'func'     => $func,
            'inputId'  => $inputId
        ];
        $this->assign('info',$info);
        //侧边栏目录栏目
        $dirArr  = $this->getDir('uploads');
        $dirArr2 = [];
        foreach ($dirArr as $key => $val) {
            $dirArr2[$val['id']] = $val['dirpath'];
        }
        foreach ($dirArr as $key => $val) {
            $countFile = 0;
            $dirfileArr = glob("{$val['dirpath']}/*");
            if (empty($dirfileArr)) {
                empty($dirfileArr) && @rmdir($val['dirpath']);
                $dirArr[$key] = [];
                continue;
            }
            $dirfileArr2 = glob("{$val['dirpath']}/*.*"); // 文件数量
            $countFile = count($dirfileArr2);
            $dirname = preg_replace('/([^\/]+)$/i', '', $val['dirpath']);
            $arr_key = array_search(trim($dirname, '/'), $dirArr2);
            if (!empty($arr_key)) {
                $dirArr[$key]['pId'] = $arr_key;
            } else {
                $dirArr[$key]['pId'] = 0;
            }
            $dirArr[$key]['name'] = preg_replace('/^(.*)\/([^\/]+)$/i', '${2}', $val['dirpath']);
            !empty($countFile) && $dirArr[$key]['name'] .= "({$countFile})";
        }
        $zNodes = str_replace("\\/", "/", json_encode($dirArr,true));
        $this->assign('zNodes', $zNodes);
        return $this->fetch('picturelist');
    }

    //图库在线管理 - 图片列表显示
    public function get_images_path($images_path = 'uploads')
    {
       if ('uploads' != $images_path && !preg_match('#^(uploads)/(.*)$#i', $images_path)) {
            $this->Notice('非法访问！');
        }
        $func = input('func/s');
        $num  = input('num/d','1');
        $info = array(
            'num'  => $num,
            'func' => empty($func) ? 'undefined' : $func,
        );
        $this->assign('info',$info);
        // 常用图片
        $common_pic = [];
        $arr1 = explode('/', $images_path);

        // 只有一级目录才显示常用图片
        if (1 >= count($arr1)) { 
            $common_pic = Db::name('common_pic')->order('id desc')->limit(9)->field('pic_path')->select()->toArray();
        }
        $this->assign('common_pic', $common_pic);
        $images_pic = Db::name('images')->order('id desc')->limit(16)->field('url')->select()->toArray();
        $this->assign('images_pic', $images_pic);
        // 图片列表
        $images_data = glob($images_path.'/*');
        $list = [];
        if (!empty($images_data)) {
            // 图片类型数组
            $image_type = sysConfig('basic.image_type');
            $image_ext = str_replace('|', ',', $image_type);
            $image_ext = explode(",",$image_ext);
            // 处理图片
            foreach ($images_data as $key => $file) {
                $fileArr = explode('.', $file);    
                $ext = end($fileArr);
                $ext = strtolower($ext);
                if (in_array($ext, $image_ext)) {
                    $list[$key]['path'] = '/'.$file;
                    $list[$key]['time'] = @filemtime($file);
                }
            }
        }
        // 图片选择的时间从大到小排序
        $list_time = get_arr_column($list,'time');
        array_multisort($list_time,SORT_DESC,$list);

        // 返回数据
        $this->assign('list', $list);
        $this->assign('path_directory', $images_path);
        return $this->fetch('get_imgpath');
    }

    //记录常用图片
    public function update_pic()
    {
        if(Request::isAjax()){
            $param = input('param.');
            if (!empty($param['images_array'])) {
                $where = '';
                $data  = []; 
                foreach ($param['images_array'] as $key => $value) {
                    // 删除条件
                    if ($key > 0) { 
                        $where .= ','; 
                    }
                    $where .= $value;
                    // 添加数组
                    $data[$key] = [
                        'pid' => md5($value),
                        'pic_path'    => $value,
                        'add_time'    => getTime(),
                        'update_time' => getTime()
                    ];
                }
                $whereor = explode(",",$where);
                // 批量删除选中的图片
                Db::name('common_pic')->where('pic_path','IN',$whereor)->delete();
                // 批量添加图片
                if(!empty($data)){
                   Db::name('common_pic')->insertAll($data);
                } 
                // 查询最后一条数据
                $row = Db::name('common_pic')->field('id')->order('id', 'desc')->limit(20,1)->select()->toArray();
                if (!empty($row)) {
                    $id = $row[0]['id'];
                    // 删除ID往后的数据
                    Db::name('common_pic')->where('id','<',$id)->delete();
                }
            }
        }
    }

    // 列出图片 
    private function fileList($allowFiles, $listSize, $get)
    {
        $dirname = '/uploads/';
        $allowFiles = substr(str_replace(".","|",join("",$allowFiles)),1);
        // 获取参数
        $size = isset($get['size']) ? htmlspecialchars($get['size']) : $listSize;
        $start = isset($get['start']) ? htmlspecialchars($get['start']) : 0;
        $end = $start + $size;
        // 获取文件列表
        $files = $this->getFiles($dirname,$allowFiles);
        if(empty($files)){
            return json_encode(["state" => "fail", "list" => [], "start" => $start, "total" => 0]);
        }
        // 获取指定范围的列表
        $len = count($files);
        for($i = min($end, $len) - 1, $list = []; $i < $len && $i >= 0 && $i >= $start; $i--){
            $list[] = $files[$i];
        }
        // 返回数据
        $result = json_encode(["state" => "SUCCESS", "list" => $list, "start" => $start, "total" => count($files)]);
        return $result;
    }

    // 遍历获取目录下的指定类型的文件 
    private function getFiles($path, $allowFiles, &$files = [])
    {
        if(!is_dir($path)) return null;
        if(substr($path,strlen($path)-1) != '/') $path .= '/';
        $handle = opendir($path);
        while(false !== ($file = readdir($handle))){ 
            if($file != '.' && $file != '..'){
                $path2 = $path.$file;
                if(is_dir($path2)){
                    $this->getFiles($path2,$allowFiles,$files);
                }else{
                    if(preg_match("/\.(".$allowFiles.")$/i",$file)){
                        $files[] = [
                            'url' => substr('/'.$path2,1),
                            'mtime' => filemtime($path2)
                        ];
                    }
                }
            }
        }      
        return $files;
    }


    // 抓取远程图片
    private function saveRemote($config,$fieldName)
    {
        $imgUrl = htmlspecialchars($fieldName);
        $imgUrl = str_replace("&amp;","&",$imgUrl);
        //http开头验证
        if(strpos($imgUrl,"http") !== 0){
            $data=array('state' => '链接不是http链接',);
            return json_encode($data);
        }
        //获取请求头并检测死链
        $heads = get_headers($imgUrl);
        if(!(stristr($heads[0],"200") && stristr($heads[0],"OK"))){
            $data=array('state' => '链接不可用',);
            return json_encode($data);
        }
        //格式验证(扩展名验证和Content-Type验证)
        if(preg_match("/^http(s?):\/\/mmbiz.qpic.cn\/(.*)/", $imgUrl) != 1){
            $fileType = strtolower(strrchr($imgUrl,'.'));
            if(!in_array($fileType,$config['allowFiles']) || (isset($heads['Content-Type']) && stristr($heads['Content-Type'],"image"))){
                $data=array('state' => '链接contentType不正确',);
                return json_encode($data);
            }
        }
        //打开输出缓冲区并获取远程图片
        ob_start();
        $context = stream_context_create(
            array('http' => array(
                'follow_location' => false
            ))
        );
        readfile($imgUrl,false,$context);
        $img = ob_get_contents();
        ob_end_clean();
        preg_match("/[\/]([^\/]*)[\.]?[^\.\/]*$/",$imgUrl,$m);
        $dirname = '/uploads/ueditor/'.date('Ymd/');
        $file['oriName'] = $m ? $m[1] : "";
        $file['filesize'] = strlen($img);
        $file['ext'] = strtolower(strrchr($config['oriName'],'.'));
        $file['name'] = uniqid().$file['ext'];
        $file['fullName'] = $dirname.$file['name'];
        $fullName = $file['fullName'];
        //检查文件大小是否超出限制
        if($file['filesize'] >= ($config["maxSize"])){
            $data=array('state' => '文件大小超出网站限制',);
            return json_encode($data);
        }
        //创建目录失败
        if(!file_exists($dirname) && !mkdir($dirname,0777,true)){
            $data=array('state' => '目录创建失败',);
            return json_encode($data);
        }else if(!is_writeable($dirname)){
            $data=array('state' => '目录没有写权限',);
            return json_encode($data);
        }
        //移动文件
        if(!(file_put_contents($fullName, $img) && file_exists($fullName))){
            $data=array('state' => '写入文件内容错误',);
            return json_encode($data);
        }else{
            print_water($file['fullName']);
            $data=array(
                'state' => 'SUCCESS',
                'url' => $file['fullName'], // 支持子目录
                'title' => $file['name'],
                'original' => $file['oriName'],
                'type' => $file['ext'],
                'size' => $file['filesize'],
            );
        }
        return json_encode($data);
    }

    /**
     * 提取上传图片目录下的所有图片
     * @param string $directory 目录路径
     * @param string $dir_name 显示的目录前缀路径
     * @param array $arr_file 是否删除空目录
     * @param num $num 数量
     */
    private function getDir($directory, &$arr_file = [], $num = 0) 
    {
        $mydir = glob($directory.'/*', GLOB_ONLYDIR);
        $param = input('param.');
        if(!isset($param['func'])){
            $func='';
        }else{
            $func='&func='.$param['func'];
        }
        if(!isset($param['num'])){
            $pnum='';
        }else{
            $pnum='?num='.$param['num'];
        }
        $request = Request::instance();
        $domain = $request->baseFile().'/ueditor/get_images_path';
        if (0 <= $num) {
            $dirpathArr = explode('/', $directory);
            $level = count($dirpathArr);
            $open = (1 >= $level) ? true : false;
            $fileList = glob($directory.'/*');
            $total = count($fileList); // 目录是否存在任意文件，否则删除该目录
            if (!empty($total)) {
                $isExistPic = $this->isExistPic($directory);
                if (!empty($isExistPic)) {
                    $arr_file[] = [
                        'id'        => $num,
                        'url'       => $domain.$pnum.$func.'&images_path='.$directory,
                        'target'    => 'content_body',
                        'isParent'  => true,
                        'open'      => $open,
                        'dirpath'   => $directory,
                        'level'     => $level,
                        'total'     => $total,
                    ];
                }
            } else {
                @rmdir("$directory");
            }
        }
        if (!empty($mydir)) {
            foreach ($mydir as $key => $dir) {
                if (stristr("$dir/", 'uploads/soft_tmp/')) {
                    continue;
                }
                $num++;
                $dirname = str_replace('\\', '/', $dir);
                $dirArr  = explode('/', $dirname);
                $dir     = end($dirArr);
                $mydir2  = glob("$directory/$dir/*", GLOB_ONLYDIR);
                if(!empty($mydir2) AND ($dir != ".") AND ($dir != "..")){
                    $this->getDir("$directory/$dir", $arr_file, $num);
                }else if(($dir != ".") AND ($dir != "..")){
                    $dirpathArr = explode('/', "$directory/$dir");
                    $level = count($dirpathArr);
                    $fileList = glob("$directory/$dir/*"); // 目录是否存在任意文件，否则删除该目录
                    $total = count($fileList);
                    if (!empty($total)) {
                         // 目录是否存在图片文件，否则删除该目录
                        $isExistPic = $this->isExistPic("$directory/$dir");
                        if (!empty($isExistPic)) {
                            if(empty(PUBLIC_ROOT)){
                                $arr_file[] = [
                                    'id'        => $num,
                                    'url'       => $domain.$pnum.$func.'&images_path='.$directory.'/'.$dir,
                                    'target'    => 'content_body',
                                    'isParent'  => false,
                                    'open'      => false,
                                    'dirpath'   => "$directory/$dir",
                                    'level'     => $level,
                                    'icon'      => $request->domain().'/common/plugins/ztree/css/zTreeStyle/img/dir_close.png',
                                    'iconOpen'  => $request->domain().'/common/plugins/ztree/css/zTreeStyle/img/dir_open.png',
                                    'iconClose' => $request->domain().'/common/plugins/ztree/css/zTreeStyle/img/dir_close.png',
                                    'total'     => $total,
                                ];
                            }else{
                                $arr_file[] = [
                                    'id'        => $num,
                                    'url'       => $domain.$pnum.$func.'&images_path='.$directory.'/'.$dir,
                                    'target'    => 'content_body',
                                    'isParent'  => false,
                                    'open'      => false,
                                    'dirpath'   => "$directory/$dir",
                                    'level'     => $level,
                                    'icon'      => $request->domain().'/'.PUBLIC_ROOT.'/common/plugins/ztree/css/zTreeStyle/img/dir_close.png',
                                    'iconOpen'  => $request->domain().'/'.PUBLIC_ROOT.'/common/plugins/ztree/css/zTreeStyle/img/dir_open.png',
                                    'iconClose' => $request->domain().'/'.PUBLIC_ROOT.'/common/plugins/ztree/css/zTreeStyle/img/dir_close.png',
                                    'total'     => $total,
                                ];
                            }
                        }
                    } else {
                        @rmdir("$directory/$dir");
                    }
                }
            }
        }
        return $arr_file;
    }
    
    /**
     * 检测指定目录是否存在图片
     * @param string $directory 目录路径
     * @param string $dir_name 显示的目录前缀路径
     * @param array $arr_file 是否删除空目录
     */
    private function isExistPic($directory, $dir_name='', &$arr_file = [])
    {
        if (!file_exists($directory) ) {
            return false;
        }
        if (!empty($arr_file)) {
            return true;
        }
        //图片类型数组
        $image_type = sysConfig('basic.image_type');
        $image_ext = str_replace('|', ',', $image_type);
        $image_ext = explode(",",$image_ext);
        $mydir = dir($directory);
        while($file = $mydir->read())
        {
            if((is_dir("$directory/$file")) AND ($file != ".") AND ($file != "..")){
                if ($dir_name) {
                    return $this->isExistPic("$directory/$file", "$dir_name/$file", $arr_file);
                } else {
                    return $this->isExistPic("$directory/$file", "$file", $arr_file);
                }
            }
            else if(($file != ".") AND ($file != "..")){
                $fileArr = explode('.', $file);    
                $ext = end($fileArr);
                $ext = strtolower($ext);
                if (in_array($ext, $image_ext)) {
                    if ($dir_name) {
                        $arr_file[] = "$dir_name/$file";
                    } else {
                        $arr_file[] = "$file";
                    }
                    return true;
                }
            }
        }
        $mydir->close();
        return $arr_file;
    }

}