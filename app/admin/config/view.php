<?php
/***********************************************************
 * 模板设置
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/

if(empty(PUBLIC_ROOT)){
    return [
        // 模板后缀
        'view_suffix'  => 'html',
        // 模板路径
        'view_path'    => app_path('view'),
        // 视图输出字符串内容替换
        'tpl_replace_string'  => [
            '{__PUBLIC_PATH}' =>  request()->domain(),  //public 目录
            '{__STATIC_PATH}' =>  request()->domain().'/static', //全局静态目录
            '{__ADMIN_PATH}'  =>  request()->domain().'/admin',  //后台目录
        ]
    ];
}else{
    return [
        // 模板后缀
        'view_suffix'  => 'html',
        // 模板路径
        'view_path'    => app_path('view'),
        // 视图输出字符串内容替换
        'tpl_replace_string'  => [
            '{__PUBLIC_PATH}' =>  request()->domain().'/'.PUBLIC_ROOT,  //public 目录
            '{__STATIC_PATH}' =>  request()->domain().'/'.PUBLIC_ROOT.'/static', //全局静态目录
            '{__ADMIN_PATH}'  =>  request()->domain().'/'.PUBLIC_ROOT.'/admin',  //后台目录
        ]
    ];

}