<?php
/***********************************************************
 * 接口请求
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\api\controller;
use think\facade\Db;
use think\facade\Cache;
use think\facade\Request;
use think\facade\Session;
use think\captcha\facade\Captcha;
class Ajax extends Base
{
    public function _initialize() {
        parent::_initialize();
    }

    // 内容页浏览量
    public function arcclick(){
        if (Request::isAjax()) {
            $aid = input('aid/d', 0);
            $click = 0;
            if (empty($aid)) {
                return $click;
            }
            if ($aid > 0) {
                Db::name('archives')->where('aid', $aid)->inc('click')->update();
                $click = Db::name('archives')->where('aid',$aid)->value('click');
            }
            return $click;
        }
    }

    // 获取表单数据信息
    public function form_submit(){
        $form_id = input('post.form_id/d');
        if (Request::isPost() && !empty($form_id))
        {
            $map = [];
            $post = input('post.');
            $ip = clientIP();
            $map[] = ['ip','=',$ip];
            $map[] = ['form_id','=',$form_id];
            $map[] = ['add_time','<',time() - 60];
            $count = Db::name('guestbook_list')->where($map)->count('list_id');
            if ($count > 0 && 1>1) {
                $result = ['status' => false, 'msg' => '同一个IP在60秒之内不能重复提交！'];
                return json($result);
            }            
            $p = Db::name('guestbook')->where('id',$form_id)->find();
            if ($p['status'] == 0) {
                $result = ['status' => false, 'msg' => '留功能未开启！'];
                return json($result);
            }
            if ($p['need_login'] ==1) {
                $result = ['status' => false, 'msg' => '需要登陆，才可以提交！'];
                return json($result);
            }  
            $city = getCity();
            $newData = [
                'form_id'    => $form_id,
                'ip'    => $ip,
                'city' => $city,
                'add_time'  => time(),
            ];
            $data = array_merge($post, $newData);
            if(Session::get('__token__') != $post['__token__']) {
                $result = ['status' => false, 'msg' => 'token验证失败！'];
                return json($result);
            }else{
                Session::set('__token__','');
            }
            unset($data['__token__']);
            $formlistRow = [];
            // 处理是否重复表单数据的提交
            $formdata = $data;
            foreach ($formdata as $key => $val) {
                if (in_array($key, ['form_id']) || preg_match('/^attr_(\d+)$/i', $key)) {
                    continue;
                }
                unset($formdata[$key]);
            }
            $md5data = md5(serialize($formdata));
            $newData['md5data'] = $md5data;
            $formlistRow = Db::name('guestbook_list')->field('list_id')->where('md5data',$md5data)->find();
            if (empty($formlistRow)) { 
                // 非重复表单的才能写入数据库
                $list_id = Db::name('guestbook_list')->insertGetId($newData);
                if ($list_id > 0) {
                    $this->saveFormValue($list_id, $form_id);
                    $result = ['status' => true, 'msg' => '提交成功'];
                    return json($result);                    
                }else{
                    $result = ['status' => true, 'msg' => '提交失败'];
                    return json($result);  
                }
            } else {
                // 存在重复数据的表单，将在后台显示在最前面
                $list_id = $formlistRow['list_id'];
                Db::name('guestbook_list')->where('list_id',$list_id)->update([
                    'is_read'   => 0,
                    'update_time'   => time(),
                ]);
                $result = ['status' => true, 'msg' => '提交成功'];
                return json($result);   
            }
        }
    }
    
    // 给指定报名信息添加表单值到 form_value
    private function saveFormValue($list_id, $form_id){
        $post = input("post.");
        foreach($post as $key => $val)
        {
            if(!strstr($key, 'attr_')){
                continue;
            } 
            $attr_id = str_replace('attr_', '', $key);
            is_array($val) && $val = implode(',', $val);
            $val = trim($val);
            $attr_value = stripslashes(filter_line_return($val, '。'));
            $adddata = array(
                'form_id'   => $form_id,
                'list_id'   => $list_id,
                'attr_id'   => intval($attr_id),
                'attr_value'   => $attr_value,
                'add_time'   => time(),
            );
            Db::name('guestbook_value')->save($adddata);
        }
    }
    
    // 验证码
    public function captcha(){
        ob_clean();
        return Captcha::create('captcha');
    }
    
}