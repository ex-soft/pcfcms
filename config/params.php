<?php
return [

    //核心字符串
    'service_pcf' => "aHR0cDovL3d3dy5wY2ZjbXMuY29tLw==",

    // 网络图片扩展名
    'image_ext' => 'jpg,jpeg,gif,bmp,ico,png,webp',

    // 登录有效期
    'login_expire' => 86400,

    'web_exception' => 0, // 关闭php所有错误

    //权限提示
    'auth_msg' => [
        'test'      => false,
        'pcfcms'    => "演示站点，禁止操作",
        'list'      => "查看权限不足", 
        'add'       => "添加权限不足",
        'modify'    => "修改权限不足",
        'delete'    => "删除权限不足",
        'status'    => "修改状态权限不足",
        'export'    => "备份权限不足",
        'optimze'   => "优化权限不足",
        'repair'    => "修复权限不足",
        'import'    => "恢复权限不足",
        'down'      => "下载权限不足",    
        'other'     => "权限不足",
    ]

];
